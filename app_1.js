const express = require('express');
const app = express();
const fs = require('fs');
const bodyParser = require('body-parser');

var logger = require('morgan');
var dir = 'logs';
if (!fs.existsSync(dir)){
  fs.mkdirSync(dir);
}

var accessLogStream = fs.createWriteStream(`./${dir}/myapplication.log`, { flags: 'a' })

app.use(logger({ stream: accessLogStream }))
app.use(bodyParser.json());
console.log(fs.existsSync("./logs/myapplication.log"))

var jsonParser = bodyParser.json()
const users = [];

function logMessage(message) {
  fs.appendFile('./logs/myapplication.log', message + '\n', (err) => {
    if (err) {
      console.error(err);
    }
  });
}


app.get('/', (req, res) => {
  logMessage('Main menu was called')
  res.send('Hello World!');
});

app.get('/users', (req, res) => {
  logMessage('Get all users')
  res.json(users);
});

app.post('/users', jsonParser, (req, res) => {
  const user = req.body;
  logMessage('Save the user: ' + JSON.stringify(user))
  users.push(user);
  res.json(user);
});

app.get('/users/:id', (req, res) => {
  const id = req.params.id;
  logMessage('Find user with id' + JSON.stringify(id))
  const user = users.find(u => u.id === id);
  if (user) {
    res.json(user);
  } else {
    logMessage('User with id ' + JSON.stringify(id) + ' not found')
    res.status(404).send(`User with id ${id} not found`);
  }
});

app.put('/users/:id', (req, res) => {
  const id = req.params.id;
  logMessage('Update user with id: ' + JSON.stringify(id))
  const userIndex = users.findIndex(u => u.id === id);
  if (userIndex === -1) {
    logMessage('User with id ' + JSON.stringify(id) + ' not found')
    res.status(404).send(`User with id ${id} not found`);
  } else {
    const updatedUser = req.body;
    users[userIndex] = updatedUser;
    res.json(updatedUser);
  }
});

app.delete('/users/:id', (req, res) => {
  const id = req.params.id;
  logMessage('Delete user with id' + JSON.stringify(id))
  const userIndex = users.findIndex(u => u.id === id);
  if (userIndex === -1) {
    logMessage('User with id ' + JSON.stringify(id) + ' not found')
    res.status(404).send(`User with id ${id} not found`);
  } else {
    const deletedUser = users.splice(userIndex, 1);
    res.json(deletedUser);
  }
});

app.listen(8001, () => {
  console.log('Server listening on port 8001');
});
