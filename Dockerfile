# Use the official node image as the base image
FROM node:14-alpine

# Set the working directory in the container
WORKDIR /app

# Copy the package.json and package-lock.json
COPY package*.json ./

# Install the dependencies
RUN npm install

# Copy the source code
COPY . .

# Expose port 8001 for the application
EXPOSE 8001

# copy in source code
COPY --chown=node:node ./ ./

# start express server
CMD [ "npm", "start" ]